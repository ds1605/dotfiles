(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("02f57ef0a20b7f61adce51445b68b2a7e832648ce2e7efb19d217b6454c1b644" default))
 '(safe-local-variable-values
   '((encoding . utf-8)
     (lsp-lua-runtime-path .
      ["?.lua" "?/init.lua" "?/?.lua" "/usr/share/awesome/lib/?.lua" "/usr/share/awesome/lib/?/?.lua"])))
 '(warning-suppress-log-types '((lsp-mode) (lsp-mode) (defvaralias)))
 '(warning-suppress-types '((lsp-mode) (defvaralias))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ts-fold-replacement-face ((t (:foreground nil :box nil :inherit font-lock-comment-face :weight light)))))
