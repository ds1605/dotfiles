#!/usr/bin/env bash

rm -rf "${HOME}/.zshrc"
ln -sr zshrc "${HOME}/.zshrc"
ln -sr plugins "${HOME}/.zsh_plugins"
